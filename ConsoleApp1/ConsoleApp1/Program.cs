﻿using System;
using Microsoft.Win32;

class Program
{
    static void Main()
    {
        //GetStartupPrograms();
        //AddToStartup("Intel Extreme Tuning", @"C:\Program Files\Intel\Intel(R) Extreme Tuning Utility\Client\PerfTune");
        //GetStartupPrograms();

        //GetScheduledTasks();
        
        ExportRegistryKeyToFile(@"Software\Microsoft\Windows\CurrentVersion\Run", "C:\\Users\\pavlo\\source\\repos\\sysproglab04\\REG");

    }

    static void GetStartupPrograms()
    {
        string[] regKeys = {
            @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run",
            @"SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Run",
        };

        foreach (var regKey in regKeys)
        {
            try
            {
                using (RegistryKey key = Registry.CurrentUser.OpenSubKey(regKey))
                {

                    if (key != null)
                    {
                        Console.WriteLine($"Список програм та служб, які завантажуються автоматично ({regKey}):");
                        
                        foreach (var valueName in key.GetValueNames())
                        {
                            string value = key.GetValue(valueName)?.ToString();
                            Console.WriteLine($"{valueName}: {value}");
                        }
                        Console.WriteLine();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Помилка при звертанні до реєстру: {ex.Message}");
            }
        }
    }
    static void AddToStartup(string appName, string appPath)
    {
        string regKeyPath = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";

        try
        {
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey(regKeyPath, true))
            {
                if (key != null)
                {
                    key.SetValue(appName, appPath);
                    Console.WriteLine($"Програма {appName} додана до автозавантаження для поточного користувача.");
                }
                else
                {
                    Console.WriteLine($"Не вдалося відкрити гілку реєстру {regKeyPath} для поточного користувача.");
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Помилка при звертанні до реєстру: {ex.Message}");
        }
    }

    static void GetScheduledTasks()
    {
        Console.WriteLine("Список завдань планувальника задач для усіх користувачів:");

        // Гілка реєстру для усіх користувачів
        string regKeyPathAllUsers = @"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Schedule\TaskCache\Tree";

        try
        {
            using (RegistryKey key = Registry.LocalMachine.OpenSubKey(regKeyPathAllUsers))
            {
                if (key != null)
                {
                    foreach (var taskName in key.GetSubKeyNames())
                    {
                        Console.WriteLine($"Завдання: {taskName}");
                    }
                }
                else
                {
                    Console.WriteLine($"Не вдалося відкрити гілку реєстру {regKeyPathAllUsers} для усіх користувачів.");
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Помилка при звертанні до реєстру: {ex.Message}");
        }

        Console.WriteLine("\nСписок завдань планувальника задач для поточного користувача:");

        // Гілка реєстру для поточного користувача
        string regKeyPathCurrentUser = @"SOFTWARE\Microsoft\Windows\CurrentVersion\TaskSchedulers\Tasks";

        try
        {
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey(regKeyPathCurrentUser))
            {
                if (key != null)
                {
                    foreach (var taskName in key.GetSubKeyNames())
                    {
                        Console.WriteLine($"Завдання: {taskName}");
                    }
                }
                else
                {
                    Console.WriteLine($"Не вдалося відкрити гілку реєстру {regKeyPathCurrentUser} для поточного користувача.");
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Помилка при звертанні до реєстру: {ex.Message}");
        }
    }

    static void ExportRegistryKeyToFile(string regKeyPath, string outputPath)
    {
        try
        {
            using (RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Default).OpenSubKey(regKeyPath))
            {
                if (key != null)
                {
                    string regContent = $"Windows Registry Editor Version 5.00{Environment.NewLine}{Environment.NewLine}";

                    foreach (var valueName in key.GetValueNames())
                    {
                        string valueData = key.GetValue(valueName)?.ToString() ?? "";
                        regContent += $"[{regKeyPath}\\{valueName}]{Environment.NewLine}\"{valueName}\"={valueData}{Environment.NewLine}";
                    }
                    string directoryPath = "C:\\Users\\pavlo\\source\\repos\\sysproglab04\\REG";
                    System.IO.Directory.CreateDirectory(directoryPath);
                    System.IO.File.WriteAllText(System.IO.Path.Combine(directoryPath, "exported_reg_file.reg"), regContent);
                    Console.WriteLine($"Розділ реєстру {regKeyPath} збережено у файл {outputPath}");
                }
                else
                {
                    Console.WriteLine($"Не вдалося відкрити розділ реєстру {regKeyPath}");
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Помилка при звертанні до реєстру: {ex.Message}");
        }
    }

    static RegistryHive GetBaseKey(string regKeyPath)
    {
        return (RegistryHive)Enum.Parse(typeof(RegistryHive), regKeyPath.Split('\\')[0]);
    }

    static string GetSubKeyPath(string regKeyPath)
    {
        string[] parts = regKeyPath.Split('\\');
        return string.Join("\\", parts, 1, parts.Length - 1);
    }
}
